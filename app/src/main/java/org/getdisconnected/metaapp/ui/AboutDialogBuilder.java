// SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package org.getdisconnected.metaapp.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import org.getdisconnected.metaapp.BuildConfig;
import org.getdisconnected.metaapp.R;

public class AboutDialogBuilder {

    public static void create(Context context){
        String msg = context.getString(R.string.about_message);
        String versionInfo = BuildConfig.VERSION_NAME;
        if (!"release".equals(BuildConfig.BUILD_TYPE)) {
            versionInfo += " (" + BuildConfig.BUILD_TYPE + ")";
        }
        msg = msg.replace("{versionInfo}", versionInfo);
        Spanned spannedMsg = Build.VERSION.SDK_INT >= 24 ? Html.fromHtml(msg, Html.FROM_HTML_MODE_COMPACT) : Html.fromHtml(msg);
        TextView tvAboutContent = new TextView(context);
        int dp25 = Math.round(25f*(context.getResources().getDisplayMetrics().xdpi / DisplayMetrics.DENSITY_DEFAULT));
        int dp10 = Math.round(10f*(context.getResources().getDisplayMetrics().xdpi / DisplayMetrics.DENSITY_DEFAULT));
        tvAboutContent.setPadding(dp25, dp10, dp25, dp10);
        tvAboutContent.setText(spannedMsg);
        tvAboutContent.setMovementMethod(LinkMovementMethod.getInstance());
        new AlertDialog.Builder(context)
                .setTitle(R.string.about_title)
                .setIcon(R.mipmap.ic_launcher)
                .setView(tvAboutContent)
                .setNegativeButton(R.string.about_close, new AlertDialog.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                }).create().show();
    }

}
