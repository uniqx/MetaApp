// SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package org.getdisconnected.metaapp.ui;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.getdisconnected.metaapp.R;
import org.getdisconnected.metaapp.api.ApiManger;
import org.getdisconnected.metaapp.api.CalendarApi;

import java.util.List;


public class CalendarRecyclerViewAdapter extends RecyclerView.Adapter<CalendarRecyclerViewAdapter.ViewHolder> {

    private final CalendarApi calendarApi;
    private final Context context;
    private List<CalendarApi.CalendarItem> calendarItems;

    public CalendarRecyclerViewAdapter(Context context) {
        this.context = context;
        this.calendarApi = ApiManger.getCalendarApi(context);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        calendarApi.addUpdateListener(calendarListener);
        calendarApi.requestCalenderUpdate();
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        calendarApi.removeUpdateListener(calendarListener);
        super.onDetachedFromRecyclerView(recyclerView);
    }

    private final CalendarApi.CalendarListener calendarListener = new CalendarApi.CalendarListener() {
        @Override
        public void umbrellaUpdated(List<CalendarApi.CalendarItem> calendarItems) {
            CalendarRecyclerViewAdapter.this.calendarItems = calendarItems;
            CalendarRecyclerViewAdapter.this.notifyDataSetChanged();
        }
    };

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_calendaritem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final CalendarApi.CalendarItem calendarItem = calendarItems.get(position);
        if (calendarItem != null) {
            holder.date.setText(calendarItem.getDate());
            holder.day.setText(calendarItem.getDay());
            holder.time.setText(calendarItem.getStartTime() + " - " + calendarItem.getEndTime());
            holder.title.setText(calendarItem.getTitle());
            holder.room.setText(calendarItem.getRoom());
            holder.teaser.setText(calendarItem.getTeaser());
            holder.listItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(calendarItem.getUrl()));
                    context.startActivity(i);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (calendarItems == null) {
            return 0;
        } else {
            return calendarItems.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View listItem;
        public final TextView date;
        public final TextView day;
        public final TextView time;
        public final TextView title;
        public final TextView room;
        public final TextView teaser;

        public ViewHolder(View view) {
            super(view);
            date = view.findViewById(R.id.date);
            day = view.findViewById(R.id.day);
            listItem = view.findViewById(R.id.list_item);
            title = view.findViewById(R.id.title);
            time = view.findViewById(R.id.time);
            room = view.findViewById(R.id.room);
            teaser = view.findViewById(R.id.teaser);
        }
    }
}
