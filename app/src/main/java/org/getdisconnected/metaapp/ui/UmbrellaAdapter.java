// SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package org.getdisconnected.metaapp.ui;

import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorChangedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

import org.getdisconnected.metaapp.R;
import org.getdisconnected.metaapp.api.UmbrellaApi;
import org.getdisconnected.metaapp.data.UmbrellaApiPrefs;

class UmbrellaAdapter extends RecyclerView.Adapter<UmbrellaAdapter.UmbrellaAdapterViewHolder> {

    private final UmbrellaApi umbrellaApi;
    private final int[] colors = new int[UmbrellaApiPrefs.UMBRELLA_COUNT];
    private final int[] warmWhites = new int[UmbrellaApiPrefs.UMBRELLA_COUNT];
    private final int[] coldWhites = new int[UmbrellaApiPrefs.UMBRELLA_COUNT];
    private final UmbrellaApiPrefs prefs;

    UmbrellaAdapter(UmbrellaApiPrefs prefs, UmbrellaApi umbrellaApi) {
        for (int i=0; i<colors.length; i++) {
            colors[i] = 0xff000000;
            warmWhites[i] = 0xff;
            coldWhites[i] = 0xff;
        }
        this.umbrellaApi = umbrellaApi;
        this.prefs = prefs;
    }

    private int mapPos(int pos) {
        if (pos % 6 == 5) {
            return pos - 2;
        } else if (pos % 6 == 3) {
            return pos + 2;
        } else {
            return pos;
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        umbrellaApi.addUpdateListener(umbrellaUpdateListener);
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        umbrellaApi.removeUpdateListener(umbrellaUpdateListener);
        super.onDetachedFromRecyclerView(recyclerView);
    }

    private final UmbrellaApi.UmbrellaUpdateListener umbrellaUpdateListener = new UmbrellaApi.UmbrellaUpdateListener() {
        @Override
        public void umbrellaUpdated(int id, int color, int warmWhite, int coldWhite) {
            int pos = mapPos(id);
            UmbrellaAdapter.this.colors[pos] = color;
            UmbrellaAdapter.this.warmWhites[pos] = warmWhite;
            UmbrellaAdapter.this.coldWhites[pos] = coldWhite;
            UmbrellaAdapter.this.notifyDataSetChanged();
        }
    };

    private void setUmbrellaColor(int position, int selectedColor) {
        UmbrellaApi.Color c = new UmbrellaApi.Color(
                (byte)(selectedColor>>16 & 0xff),
                (byte)(selectedColor>>8 & 0xff),
                (byte)(selectedColor & 0xff),
                (byte)0,
                (byte)0
        );
        umbrellaApi.sendUmbrella(mapPos(position), c);
    }

    @NonNull
    @Override
    public UmbrellaAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_umbrella, null);
        return new UmbrellaAdapterViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull final UmbrellaAdapterViewHolder holder, final int position) {

        final int originalColor = colors[position];
        holder.umbrellaLabel.setText("" + mapPos(position));
        holder.umbrellaLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorPickerDialogBuilder
                        .with(holder.umbrellaLabel.getContext())
                        .setTitle("Choose color")
                        .initialColor(colors[position])
                        .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                        .showAlphaSlider(false)
                        .density(12)
                        .setOnColorChangedListener(new OnColorChangedListener() {
                            @Override
                            public void onColorChanged(int selectedColor) {
                                setUmbrellaColor(position, selectedColor);
                            }
                        })
                        .setPositiveButton("ok", new ColorPickerClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                            }
                        })
                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                setUmbrellaColor(position, originalColor);
                            }
                        })
                        .build()
                        .show();
            }
        });

        if (warmWhites[position] == 0 && coldWhites[position] == 0) {
            if (colors[position] == 0xff000000) {
                holder.umbrellaColorwheel.setVisibility(View.GONE);
                holder.umbrellaBg.setColorFilter(mapColor(colors[position]));
            } else {
                holder.umbrellaColorwheel.setVisibility(View.VISIBLE);
                holder.umbrellaBg.setColorFilter(mapColor(colors[position]));
            }
        } else {
            holder.umbrellaColorwheel.setVisibility(View.INVISIBLE);
            holder.umbrellaBg.setColorFilter(warmWhites[position] & coldWhites[position]);
        }
    }

    private int mapColor(int color) {
        return 0xff000000 | (((color&0xff)>>1)+0x80) | (((color&0xfe00)>>1)+0x8000) | (((color&0xfe0000)>>1)+0x800000);
    }

    @Override
    public int getItemCount() {
        return UmbrellaApiPrefs.UMBRELLA_COUNT;
    }

    public void onResume() {
        umbrellaApi.requestFullRefresh();
    }

    public void onPause() {
        umbrellaApi.persistColorState();
    }

    public class UmbrellaAdapterViewHolder extends RecyclerView.ViewHolder {

        TextView umbrellaLabel;
        View umbrellaLayout;
        View umbrellaLines;
        View umbrellaColorwheel;
        ImageView umbrellaBg;

        public UmbrellaAdapterViewHolder(@NonNull final View itemView) {
            super(itemView);
            umbrellaLabel = itemView.findViewById(R.id.umbrella_label);
            umbrellaLayout = itemView.findViewById(R.id.umbrella_layout);
            umbrellaLines = itemView.findViewById(R.id.umbrella_lines);
            umbrellaColorwheel = itemView.findViewById(R.id.umbrella_colorwheel);
            umbrellaBg = itemView.findViewById(R.id.umbrella_bg);
        }
    }
}
