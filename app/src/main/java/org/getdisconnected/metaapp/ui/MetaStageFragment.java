// SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package org.getdisconnected.metaapp.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import androidx.fragment.app.Fragment;

import org.getdisconnected.metaapp.R;
import org.getdisconnected.metaapp.api.GenericSonoffApi;
import org.getdisconnected.metaapp.api.ApiManger;

public class MetaStageFragment extends Fragment {

    private Switch sonne20;
    private GenericSonoffApi metastage9Api;
    private GenericSonoffApi metastage7Api;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        metastage7Api = ApiManger.getMetastage7Api(getContext());
        metastage9Api = ApiManger.getMetastage9Api(getContext());
        metastage9Api.addUpdateListener(metastage9UpdateListener);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_metastage, container, false);

        sonne20 = view.findViewById(R.id.switch_sonne20);
        sonne20.setOnClickListener(sonne20clicked);
        sonne20.setChecked(false);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        metastage9Api.requestUpdate();
    }

    private GenericSonoffApi.MetaStageUpdateListener metastage9UpdateListener = new GenericSonoffApi.MetaStageUpdateListener() {
        @Override
        public void statusUpdate(GenericSonoffApi.SonoffStatus status) {
            switch (status) {
                case ON:
                    sonne20.setChecked(true);
                    sonne20.setEnabled(true);
                    break;
                case OFF:
                    sonne20.setChecked(false);
                    sonne20.setEnabled(true);
                    break;
                case UNKNOWN:
                    sonne20.setChecked(false);
                    sonne20.setEnabled(false);
                    break;
            }
        }
    };

    private final View.OnClickListener sonne20clicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (sonne20.isChecked()) {
                metastage7Api.requestState(GenericSonoffApi.SonoffStatus.ON);
                metastage9Api.requestState(GenericSonoffApi.SonoffStatus.ON);
            } else {
                metastage7Api.requestState(GenericSonoffApi.SonoffStatus.OFF);
                metastage9Api.requestState(GenericSonoffApi.SonoffStatus.OFF);
            }
            sonne20.setEnabled(false);
        }
    };
}
