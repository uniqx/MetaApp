// SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package org.getdisconnected.metaapp.api;

import android.content.Context;

/**
 * stupid self-built dependency injection thingy.
 */
public class ApiManger {

    private static MetalabEingagnsApi hodorsCyberCoffeeApi = null;
    private static LabNetworkChecker labNetworkChecker = null;
    private static UmbrellaApi umbrellaApi = null;
    private static CalendarApi calendarApi;
    private static GenericSonoffApi metastage7Api = null;
    private static GenericSonoffApi metastage9Api = null;

    public static synchronized MetalabEingagnsApi getHodorsCyberCoffeApi(){
        if (hodorsCyberCoffeeApi == null) {
            hodorsCyberCoffeeApi = new MetalabEingagnsApi();
        }
        return hodorsCyberCoffeeApi;
    }

    public static synchronized LabNetworkChecker getLabNetworkChecker() {
        if (labNetworkChecker == null) {
            labNetworkChecker = new LabNetworkChecker();
        }
        return labNetworkChecker;
    }

    public static synchronized UmbrellaApi getUmbrellaApi(Context context) {
        if (umbrellaApi == null) {
            umbrellaApi = new UmbrellaApi(context.getApplicationContext());
        }
        return umbrellaApi;
    }

    public static synchronized CalendarApi getCalendarApi(Context context) {
        if (calendarApi == null) {
            calendarApi = new CalendarApi(context);
        }
        return calendarApi;
    }

    public static synchronized GenericSonoffApi getMetastage7Api(Context context) {
        if (metastage7Api == null) {
            metastage7Api = new GenericSonoffApi("metastage7.lan");
        }
        return metastage7Api;
    }

    public static synchronized GenericSonoffApi getMetastage9Api(Context context) {
        if (metastage9Api == null) {
            metastage9Api = new GenericSonoffApi("metastage9.lan");
        }
        return metastage9Api;
    }
}
