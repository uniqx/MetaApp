// SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package org.getdisconnected.metaapp.api;

import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbstractThreadedApi<L>{

    Handler mainHandler;
    Handler apiHandler;
    private HandlerThread apiThread;

    AbstractThreadedApi() {
        apiThread = new HandlerThread(this.getClass().getName());
        apiThread.start();
        apiHandler = new Handler(apiThread.getLooper());
        mainHandler = new Handler(Looper.getMainLooper());
    }

    final List<L> listeners = Collections.synchronizedList(new ArrayList<L>());

    public void addUpdateListener(L listener){
        listeners.add(listener);
    }

    public void removeUpdateListener(L listener){
        listeners.remove(listener);
    }

    @Override
    protected void finalize() throws Throwable {
        if (Build.VERSION.SDK_INT >= 18) {
            apiThread.quitSafely();
        } else {
            apiThread.quit();
        }
    }
}
