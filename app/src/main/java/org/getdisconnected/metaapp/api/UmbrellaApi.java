// SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package org.getdisconnected.metaapp.api;

import android.content.Context;

import org.getdisconnected.metaapp.data.UmbrellaApiPrefs;

import java.net.SocketException;

import artnet4j.ArtNet;
import artnet4j.ArtNetException;
import artnet4j.packets.ArtDmxPacket;

public class UmbrellaApi extends AbstractThreadedApi<UmbrellaApi.UmbrellaUpdateListener>{

    private final Context context;
    private final ArtNet artNet;

    private final int artnetUniverse = 3;
    private final int artnetSubnet = 0;
    private final String ip = "10.20.255.255"; // n.b. broadcast

    private final byte[] colorBuffer = new byte[UmbrellaApiPrefs.UMBRELLA_COUNT*5];

    UmbrellaApi(final Context context) {
        super();

        this.artNet = new ArtNet();
        this.context = context;

        apiHandler.post(new Runnable() {
            @Override
            public void run() {
                restorePersistedColors();

                try {
                    artNet.start();
                } catch (SocketException e) {
                    e.printStackTrace();
                } catch (ArtNetException e) {
                    e.printStackTrace();
                }

                requestFullRefresh();
            }
        });
    }

    public void braodcastFullRand() {
        apiHandler.post(new Runnable() {
            @Override
            public void run() {
                for(int i=0; i < UmbrellaApiPrefs.UMBRELLA_COUNT; i++) {
                    Color.randColor().writeTo(colorBuffer, i*5);
                    notifyUmbreallaUpdated(i);
                }
                braodcastColorBuffer();
            }
        });
    }

    private ArtDmxPacket buildPacket(int artnetSubnet, int artnetUniverse, byte[] data, int numChannels) {
        ArtDmxPacket artDmxPacket = new ArtDmxPacket();
        artDmxPacket.setUniverse(artnetSubnet, artnetUniverse);
        artDmxPacket.setSequenceID(0);
        artDmxPacket.setDMX(data, numChannels);

        return artDmxPacket;
    }

    public void restorePersistedColors() {
        apiHandler.post(new Runnable() {
            @Override
            public void run() {
                UmbrellaApiPrefs prefs = new UmbrellaApiPrefs(context);
                byte[] restored = prefs.loadColorBuffer();
                if (restored.length == UmbrellaApiPrefs.UMBRELLA_COUNT*5) {
                    for (int i=0; i<restored.length; i++) {
                        colorBuffer[i] = restored[i];
                    }
                }
            }
        });
    }

    public void braodcastWhite(final byte warmWhite, final byte coldWhite) {
        apiHandler.post(new Runnable() {
            @Override
            public void run() {
                Color w = new Color((byte) 0, (byte) 0, (byte) 0, warmWhite, coldWhite);
                for(int i=0; i < UmbrellaApiPrefs.UMBRELLA_COUNT; i++) {
                    w.writeTo(colorBuffer, i*5);
                    notifyUmbreallaUpdated(i);
                }
                braodcastColorBuffer();
            }
        });
    }

    private void braodcastColorBuffer() {
        ArtDmxPacket packetColor1 = buildPacket(artnetSubnet, artnetUniverse, colorBuffer, colorBuffer.length);
        artNet.unicastPacket(packetColor1, ip);
    }

    public void sendUmbrella(final int id, final Color color) {
        apiHandler.post(new Runnable() {
            @Override
            public void run() {
                color.writeTo(colorBuffer, id*5);
                braodcastColorBuffer();
                notifyUmbreallaUpdated(id);
            }
        });
    }

    public void persistColorState() {
        apiHandler.post(new Runnable() {
            @Override
            public void run() {
                UmbrellaApiPrefs prefs = new UmbrellaApiPrefs(context);
                prefs.storeColorBuffer(colorBuffer);
            }
        });
    }

    public static class Color{
        byte[] raw;

        public Color(byte r, byte g, byte b, byte ww, byte cw) {
            raw = new byte[]{r, g, b, ww, cw};
        }

        Color(byte[] raw){
            this.raw = raw;
        }

        void writeTo(byte[] targetArray, int offset) {
            for(int i=0; i<5; i++) {
                targetArray[offset+i] = raw[i];
            }
        }

        public int toAndroidColor() {
            return 0xff000000 | ((((int)raw[0])<<16)&0xff0000) | ((((int)raw[1])<<8)&0xff00) | (((int)raw[2])&0xff);
        }

        public static Color randColor(){
            if(Math.random() < .5) {
                return new Color(
                        (byte)(Math.random() * 255), // r
                        (byte)(Math.random() * 255), // g
                        (byte)(Math.random() * 255), // b
                        (byte)(Math.random() * 0), // ww
                        (byte)(Math.random() * 0) // cw
                );
            } else {
                if(Math.random() < .5) {
                    return new Color(
                            (byte)(127 + Math.random() * 128), // r
                            (byte)(Math.random() * 0), // g
                            (byte)(Math.random() * 0), // b
                            (byte)(Math.random() * 0), // ww
                            (byte)(Math.random() * 0) // cw
                    );
                } else {
                    return new Color(
                            (byte)(Math.random() * 0), // r
                            (byte)(127 + Math.random() * 128), // g
                            (byte)(Math.random() * 0), // b
                            (byte)(Math.random() * 0), // ww
                            (byte)(Math.random() * 0) // cw
                    );
                }
            }
        }
    }

    public void requestFullRefresh(){
        apiHandler.post(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i< UmbrellaApiPrefs.UMBRELLA_COUNT; i++) {
                    notifyUmbreallaUpdated(i);
                }
            }
        });
    }

    public interface UmbrellaUpdateListener {
        void umbrellaUpdated(int id, int color, int warmWhite, int coldWhite);
    }

    private void notifyUmbreallaUpdated(final int id) {

        final int color = 0xff000000 | ((((int)colorBuffer[id*5+0])<<16)&0xff0000) | ((((int)colorBuffer[id*5+1])<<8)&0xff00) | (((int)colorBuffer[id*5+2])&0xff);
        final int warmWhite = colorBuffer[id*5+3];
        final int coldWhite = colorBuffer[id*5+4];

        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                synchronized (listeners) {
                    for (UmbrellaUpdateListener listener : listeners) {
                        listener.umbrellaUpdated(id, color, warmWhite, coldWhite);
                    }
                }
            }
        });
    }
}
