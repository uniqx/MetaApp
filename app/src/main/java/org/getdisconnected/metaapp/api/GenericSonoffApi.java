// SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package org.getdisconnected.metaapp.api;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class GenericSonoffApi extends AbstractThreadedApi<GenericSonoffApi.MetaStageUpdateListener> {

    private final String address;

    GenericSonoffApi(String url) {
        this.address = url;
    }

    public void requestUpdate() {

        apiHandler.post(new Runnable() {
            @Override
            public void run() {
                URLConnection c;
                try {
                    URL url = new URL("http://" + address + "/?m=1");
                    c = url.openConnection();
                    c.setReadTimeout(6*1000);
                    BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));

                    String line = in.readLine();
                    if (line != null) {
                        if (line.contains("ON")) {
                            notifyStatusUpdate(SonoffStatus.ON);
                            return;
                        } else if (line.contains("OFF")) {
                            notifyStatusUpdate(SonoffStatus.OFF);
                            return;
                        }
                    }

                } catch (MalformedURLException e) {
                    Log.e("###", "could not check sonoff status", e);
                } catch (IOException e) {
                    Log.e("###", "could not check sonoff status", e);
                }

                notifyStatusUpdate(GenericSonoffApi.SonoffStatus.UNKNOWN);
            }
        });
    }


    public void requestToggle() {
        apiHandler.post(new Runnable() {
            @Override
            public void run() {
                URLConnection c;
                try {
                    URL url = new URL("http://" + address + "/?m=1&o=1");
                    c = url.openConnection();
                    c.setReadTimeout(6*1000);
                    BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));

                    String line = in.readLine();
                    if (line != null) {
                        if (line.contains("ON")) {
                            notifyStatusUpdate(SonoffStatus.ON);
                            return;
                        } else if (line.contains("OFF")) {
                            notifyStatusUpdate(SonoffStatus.OFF);
                            return;
                        }
                    }

                } catch (MalformedURLException e) {
                    Log.e("###", "could not check sonoff status after toggle", e);
                } catch (IOException e) {
                    Log.e("###", "could not check sonoff status after toggle", e);
                }

                notifyStatusUpdate(GenericSonoffApi.SonoffStatus.UNKNOWN);
            }
        });
    }

    public void requestState(final SonoffStatus state) {
        apiHandler.post(new Runnable() {
            @Override
            public void run() {
                URLConnection c;
                try {
                    URL url = new URL("http://" + address + "/?m=1");
                    c = url.openConnection();
                    c.setReadTimeout(6*1000);
                    BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    String line = in.readLine();
                    if (line != null) {
                        Log.d("###", "l-- " + line);
                        if (line.contains("ON")) {
                            if (state == SonoffStatus.ON) {
                                notifyStatusUpdate(SonoffStatus.ON);
                                return;
                            } else if (state == SonoffStatus.OFF) {
                                requestToggle();
                                return;
                            }
                        } else if (line.contains("OFF")) {
                            if (state == SonoffStatus.OFF) {
                                notifyStatusUpdate(SonoffStatus.OFF);
                                return;
                            } else if (state == SonoffStatus.ON) {
                                requestToggle();
                                return;
                            }
                        }
                    }

                } catch (MalformedURLException e) {
                    Log.e("###", "could not set sonoff status", e);
                } catch (IOException e) {
                    Log.e("###", "could not set sonoff status", e);
                }

                notifyStatusUpdate(GenericSonoffApi.SonoffStatus.UNKNOWN);
            }
        });
    }

    private void notifyStatusUpdate(final SonoffStatus status) {
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                synchronized (listeners) {
                    for (MetaStageUpdateListener listener : listeners) {
                        listener.statusUpdate(status);
                    }
                }
            }
        });
    }

    public interface MetaStageUpdateListener {
        void statusUpdate(SonoffStatus status);
    }

    public enum SonoffStatus {
        ON, OFF, UNKNOWN
    }
}
